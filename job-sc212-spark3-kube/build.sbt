scalaVersion := "2.12.18"

organization := "com.tiki"

name := "job-sc212-spark3-kube"
version := "0.2.4"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies += "com.tiki" %% "ai-common" % "1.2.0"
libraryDependencies += "org.scalatest"  %% "scalatest" % "3.2.15" % Test
// ai-core is only used for testing download of private library
libraryDependencies += "com.tiki"         %% "ai-core"   % "1.1.1"
libraryDependencies += "org.apache.spark" %% "spark-core"  % "3.4.1" % Provided
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "3.4.1" % Provided
libraryDependencies += "org.apache.hadoop.shaded.com.jcraft" % "jsch"  %  "0.1.55" % Test
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "3.3.0" % Provided

resolvers ++= Seq(
  "DSP-public" at "https://nexus.tiki-dsp.io/repository/maven-public/",
"DSP-private" at "https://nexus.tiki-dsp.io/repository/maven-private/"
)

updateOptions := updateOptions.value.withLatestSnapshots(true)

Compile / doc / sources := Seq.empty

enablePlugins(JavaAppPackaging)
