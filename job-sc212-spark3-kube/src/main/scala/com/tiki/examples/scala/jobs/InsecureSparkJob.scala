package com.tiki.examples.scala.jobs

import com.tiki.ai.job.{CLIOptionParserRunner, OptionParserRunner}
import com.tiki.ai.spark.SparkApp
import com.tiki.examples.scala.jobConfigParser.InsecureSparkJobConfig
import org.slf4j.{Logger, LoggerFactory}

import scala.math.random
import scala.util.Try


object InsecureSparkJob extends OptionParserRunner {

  override def apply(arguments: Seq[String]): Try[Unit] = new InsecureSparkJob().execute(arguments)

}


class InsecureSparkJob
  extends SparkApp[InsecureSparkJobConfig]
  with CLIOptionParserRunner[InsecureSparkJobConfig] {

  executorCores(1)
  executorCount(1)
  val log: Logger = LoggerFactory.getLogger(this.getClass)
  updateSparkConfig(_.set("spark.kryoserializer.buffer.max", "512m"))

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: InsecureSparkJobConfig): Unit = {

    val n = math.min(100000L * jobConfig.slices, Int.MaxValue).toInt

    val count = spark.sparkContext
      .parallelize(1 until n, jobConfig.slices)
      .map { i =>
        val x = random * 2 - 1
        val y = random * 2 - 1
        if (x * x + y * y <= 1) 1 else 0
      }
      .reduce(_ + _)

    log.info(s"Pi is roughly ${4.0 * count / (n - 1)}")
  }

  override def terminate(jobConfig: InsecureSparkJobConfig): Unit = {
    super.terminate(jobConfig)
    log.info("Insecure Spark job finished")
  }

  def parse(arguments: Seq[String]): InsecureSparkJobConfig = InsecureSparkJobConfig()
}

