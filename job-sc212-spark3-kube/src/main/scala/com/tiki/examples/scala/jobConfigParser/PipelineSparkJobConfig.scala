package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.ai.job.JobConfig
import scopt.OptionParser


case class PipelineSparkJobConfig(name: String = "PipelineSparkJob",
                                  pipeline: FileSystemLocation = FileSystemLocation.empty,
                                  output: FileSystemLocation = FileSystemLocation.empty)
  extends OptionParser[PipelineSparkJobConfig](name)
  with  JobConfig {

    val parser: OptionParser[PipelineSparkJobConfig] = new OptionParser[PipelineSparkJobConfig]("PipelineSparkJob")
    {
      opt[String]('n', "name").optional().valueName("<name>").action( (x, c) =>  c.copy(name = x))
        .text("Job name")

      opt[String]('p', "pipeline").optional().valueName("<pipeline>").action( (x, c) =>  c.copy(pipeline = FileSystemLocation(x)))
        .text("pipeline directory")

      opt[String]('o', "output").optional().valueName("<output>").action( (x, c) =>  c.copy(output = FileSystemLocation(x)))
        .text("output directory")
    }
}
