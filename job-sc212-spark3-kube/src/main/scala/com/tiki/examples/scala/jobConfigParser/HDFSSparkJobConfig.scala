package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.job.JobConfig
import scopt.OptionParser

case class HDFSSparkJobConfig(name: String = "HhdfsSparkJob",
                              parallelism: Option[Int] = None,
                              hdfsPath: String = "")
  extends JobConfig