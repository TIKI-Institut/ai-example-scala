package com.tiki.examples.scala.jobs

import com.tiki.ai.job.{CLIOptionParserRunner, OptionParserRunner}
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.spark.SparkApp
import com.tiki.examples.scala.jobConfigParser.SftpTestJobConfig
import org.apache.hadoop.fs.sftp.SFTPFileSystem

import org.slf4j.{Logger, LoggerFactory}
import scala.util.Try



object HadoopClientAPITestJob extends OptionParserRunner {
  def apply(arguments: Seq[String]): Try[Unit] = new HadoopClientAPITestJob().execute(arguments)

}

class HadoopClientAPITestJob extends
  SparkApp[SftpTestJobConfig]
  with CLIOptionParserRunner[SftpTestJobConfig] {

  val log: Logger = LoggerFactory.getLogger(this.getClass)
  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: SftpTestJobConfig): Unit = {
    // Test hadoop-client-api library.
    val fs = new SFTPFileSystem()
    fs.close()

    log.info("Importing hadoop client api library was successful!")
  }

  override def terminate(jobConfig: SftpTestJobConfig): Unit = {
    super.terminate(jobConfig)
    log.info("Terminating job.")
  }

  def parse(arguments: Seq[String]): SftpTestJobConfig = {
    val initialConfig = SftpTestJobConfig()
    initialConfig.withArguments(arguments)(initialConfig.parser)
  }
}

