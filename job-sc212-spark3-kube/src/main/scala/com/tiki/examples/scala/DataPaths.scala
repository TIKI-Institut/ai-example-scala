package com.tiki.examples.scala

object DataPaths {

  /** Raw input data directories (CSV, JSON) */
  object Tier0{
    val baseFolder = "00-tier0/"

    val rawData: String = baseFolder + "rawData"
  }

  /** Ingested parquet data (1st iteration) */
  object Tier1{
    val baseFolder = "01-tier1/"

    val ingestedData: String = baseFolder + "ingestedData.parquet"
  }

  /** Corpus Data */
  object Tier2{
    val baseFolder = "02-tier2/"

    val corpus: String = baseFolder + "corpus.parquet"
  }

}
