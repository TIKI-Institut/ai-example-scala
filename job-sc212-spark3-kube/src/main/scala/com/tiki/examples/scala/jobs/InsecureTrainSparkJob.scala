package com.tiki.examples.scala.jobs

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.ai.job.{CLIOptionParserRunner, OptionParserRunner}
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.spark.SparkApp
import com.tiki.examples.scala.jobConfigParser.HDFSSparkJobConfig
import org.slf4j.{Logger, LoggerFactory}

import scala.io.Source
import scala.util.Try


object InsecureTrainSparkJob extends OptionParserRunner {


  def apply(arguments: Seq[String]): Try[Unit] = new InsecureTrainSparkJob().execute(arguments)
}

class InsecureTrainSparkJob
  extends SparkApp[HDFSSparkJobConfig]
  with CLIOptionParserRunner[HDFSSparkJobConfig] {

  val log: Logger = LoggerFactory.getLogger(this.getClass)
  def uid: String = java.util.UUID.randomUUID.toString.replaceAll("-", "")

  val fs: FileSystemLocation = FileSystemLocation.root("model")
  val testDirContext: FileSystemLocation = fs.locate("target")


  updateSparkConfig(_.setMaster("local"))

  /**
   * Main method that executes business logic of the job
   */
  override def run(jobConfig: HDFSSparkJobConfig): Unit = {
    import org.apache.spark.ml.evaluation.RegressionEvaluator
    import org.apache.spark.ml.regression.{RandomForestRegressionModel, RandomForestRegressor}


    // Load and parse the data file, converting it to a DataFrame.
    val fileContents = Source.fromInputStream(getClass.getResourceAsStream("/sample_linear_regression_data.txt")).getLines().toList
    val fileRDD = spark.sparkContext.parallelize(fileContents)
    fileRDD.saveAsTextFile(jobConfig.hdfsPath)
    val data = spark.read.format("libsvm").load(jobConfig.hdfsPath)

    // Split the data into training and test sets (30% held out for testing).
    val Array(trainingData, testData) = data.randomSplit(Array(0.7, 0.3))

    // Train a RandomForest model.
    val rf = new RandomForestRegressor()
    val model = rf.fit(trainingData)
    val predictions = model.transform(testData)
    predictions.select("prediction", "label", "features").show(5)
    // Select (prediction, true label) and compute test error.
    val evaluator = new RegressionEvaluator()
      .setLabelCol("label")
      .setPredictionCol("prediction")
      .setMetricName("rmse")
    val rmse = evaluator.evaluate(predictions)
    log.info(s"Root Mean Squared Error (RMSE) on test data = $rmse")

    val modelDir = testDirContext.locate(uid)
    log.info(s"modelDir: ${modelDir.resolve.toString}")
    model.save(modelDir.resolve.toString)
    val loadedModel = RandomForestRegressionModel.load(modelDir.resolve.toString)
    log.info(s"Model loaded: ${loadedModel.toString()}")
    val predictions2 = loadedModel.transform(testData)

    // Select example rows to display.
    predictions2.select("prediction", "label", "features").show(5)
    modelDir.delete()

  }

  override def terminate(jobConfig: HDFSSparkJobConfig): Unit = {
    super.terminate(jobConfig)
    log.info("Insecure Spark job finished")
  }

  def parse(arguments: Seq[String]): HDFSSparkJobConfig = {
    HDFSSparkJobConfig(hdfsPath = arguments(1))
  }

}

