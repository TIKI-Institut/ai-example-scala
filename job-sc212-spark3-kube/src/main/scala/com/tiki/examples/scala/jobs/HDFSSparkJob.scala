package com.tiki.examples.scala.jobs

import com.tiki.ai.job.{CLIOptionParserRunner, OptionParserRunner}
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.spark.SparkApp
import com.tiki.examples.scala.jobConfigParser.HDFSSparkJobConfig
import org.apache.hadoop.fs.{FileSystem, Path}
import scopt.OptionParser

import scala.io.Source
import scala.util.Try
import org.slf4j.{Logger, LoggerFactory}


object HDFSSparkJob extends OptionParserRunner {

    def apply(arguments: Seq[String]): Try[Unit] = new HDFSSparkJob().execute(arguments)
}

class HDFSSparkJob
  extends SparkApp[HDFSSparkJobConfig]
  with CLIOptionParserRunner[HDFSSparkJobConfig] {

  executorCores(1)
  executorCount(1)
  executorMemory(1024)
  val log: Logger = LoggerFactory.getLogger(this.getClass)
  def runLocalWordCount(fileContents: List[String]): Int = {
    fileContents
      .flatMap(_.split(" "))
      .flatMap(_.split("\t"))
      .filter(_.nonEmpty)
      .groupBy(w => w)
      .mapValues(_.size)
      .values
      .sum
  }

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: HDFSSparkJobConfig): Unit = {

    val fileContents   = Source.fromInputStream(getClass.getResourceAsStream("/text.txt")).getLines().toList
    val localWordCount = runLocalWordCount(fileContents)

    val dfsFilename = jobConfig.hdfsPath

    val dfsPath = new Path(dfsFilename)

    val fs = FileSystem.get(hadoopConfiguration)
    if (fs.exists(dfsPath)) {
      log.warn("Path already exists, delete it first")
      fs.delete(dfsPath, true)
    }

    log.info("Writing local file to DFS")

    val fileRDD = spark.sparkContext.parallelize(fileContents)
    fileRDD.saveAsTextFile(dfsFilename)

    log.info("Reading file from DFS and running Word Count")
    val readFileRDD = spark.sparkContext.textFile(dfsFilename)

    val dfsWordCount = readFileRDD
      .flatMap(_.split(" "))
      .flatMap(_.split("\t"))
      .filter(_.nonEmpty)
      .map(w => (w, 1))
      .countByKey()
      .values
      .sum

    spark.stop()

    if (localWordCount == dfsWordCount) {
      log.info(
        s"Success! Local Word Count $localWordCount and " +
          s"DFS Word Count $dfsWordCount agree.")
    } else {
      log.info(
        s"Failure! Local Word Count $localWordCount " +
          s"and DFS Word Count $dfsWordCount disagree.")
    }

  }

  override def terminate(jobConfig: HDFSSparkJobConfig): Unit = {
    super.terminate(jobConfig: HDFSSparkJobConfig)
    log.info("HDFS Spark job finished")
  }


  def parse(arguments: Seq[String]): HDFSSparkJobConfig = {

    val parser = new OptionParser[HDFSSparkJobConfig]("name") {
      opt[String]("name").optional().valueName("<name>").action((x, c) => c.copy(name = x))
      opt[Option[Int]]("parallelism").optional().valueName("<parallelism>").action((x, c) => c.copy(parallelism = x))
      opt[String]("hdfs-path").optional().valueName("<hdfsPath>").action((x, c) => c.copy(hdfsPath = x))
    }


    HDFSSparkJobConfig().withArguments(arguments)(parser)
  }
}



