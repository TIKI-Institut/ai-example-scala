package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.job.JobConfig
import scopt.OptionParser

case class SftpTestJobConfig(name: String = "SftpTestJob")
  extends OptionParser[SftpTestJobConfig](name)
    with JobConfig {

      val parser: OptionParser[SftpTestJobConfig] = new OptionParser[SftpTestJobConfig]("testest")
      {
        opt[String]('n', "name").optional().valueName("<name>").action( (x, c) =>  c.copy(name = x))
          .text("Job name")
      }

}
