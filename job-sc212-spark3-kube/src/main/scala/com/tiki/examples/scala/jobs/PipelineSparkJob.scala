package com.tiki.examples.scala.jobs

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.job.{CLIOptionParserRunner, OptionParserRunner}
import com.tiki.ai.spark.SparkApp
import com.tiki.examples.scala.DataPaths
import com.tiki.examples.scala.jobConfigParser.PipelineSparkJobConfig
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, sum}
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

object PipelineSparkJob extends OptionParserRunner {
  def apply(arguments: Seq[String]): Try[Unit] = new PipelineSparkJob().execute(arguments)
}


class PipelineSparkJob
  extends SparkApp[PipelineSparkJobConfig]
  with CLIOptionParserRunner[PipelineSparkJobConfig] {

  val log: Logger = LoggerFactory.getLogger(this.getClass)
  override def initialize(jobConfig: PipelineSparkJobConfig): Unit = {
    updateSparkConfig { conf =>
      conf.set("spark.kubernetes.executor.volumes.persistentVolumeClaim.models.mount.path", "/opt/app/model")
      conf.set("spark.kubernetes.executor.volumes.persistentVolumeClaim.models.mount.readOnly", "false")
      conf.set("spark.kubernetes.executor.volumes.persistentVolumeClaim.models.options.claimName", "model")

    super.initialize(jobConfig)
    }
  }

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: PipelineSparkJobConfig): Unit = run_(jobConfig)

  private def run_(jobConfig: PipelineSparkJobConfig): Unit = {

    log.info("Starting Pipline-Job")

    ingestData(jobConfig.pipeline)
      .write
      .mode("overwrite")
      .parquet(jobConfig.pipeline.locate(DataPaths.Tier1.ingestedData).resolve.toString)

    val ingestedData = spark.read.parquet(jobConfig.pipeline.locate(DataPaths.Tier1.ingestedData).resolve.toString)

    createCorpus(ingestedData)
      .write
      .mode("overwrite")
      .parquet(jobConfig.pipeline.locate(DataPaths.Tier2.corpus).resolve.toString)

    val corpus = spark.read.parquet(jobConfig.pipeline.locate(DataPaths.Tier2.corpus).resolve.toString)

    corpus.write
      .mode("overwrite")
      .parquet(jobConfig.output.resolve.toString)

    val count = corpus.count()

    log.info(s"Successfully created corpus with $count rows")

  }

  def ingestData(inputLocation: FileSystemLocation): DataFrame = {
    spark.read
      .option("header", "true")
      .option("delimiter", ";")
      .csv(inputLocation.locate(DataPaths.Tier0.rawData).resolve.toString)
  }

  def createCorpus(df: DataFrame): DataFrame = {
    df
      .groupBy(col("A"))
      .agg(sum(col("B")).alias("sum_B"),
        sum(col("C")).alias("sum_C"))
  }

  def parse(arguments: Seq[String]): PipelineSparkJobConfig = {
    val initialConfig = PipelineSparkJobConfig()
    initialConfig.withArguments(arguments)(initialConfig.parser)
  }


  override def terminate(jobConfig: PipelineSparkJobConfig): Unit = {
    super.terminate(jobConfig)
  }
}

