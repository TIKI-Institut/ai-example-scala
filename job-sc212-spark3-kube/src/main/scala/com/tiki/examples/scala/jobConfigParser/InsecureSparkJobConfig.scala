package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.job.JobConfig
import scopt.OptionParser

case class InsecureSparkJobConfig(name: String = "InsecureSparkJob",
                                  slices: Int = 10)
    extends JobConfig
