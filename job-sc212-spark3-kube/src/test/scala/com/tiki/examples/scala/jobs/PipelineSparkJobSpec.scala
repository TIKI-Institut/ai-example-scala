package com.tiki.examples.scala.jobs

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.examples.scala.{DataPaths, SparkTestSession}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PipelineSparkJobSpec
  extends AnyFlatSpec
  with Matchers
  with BeforeAndAfterAll
  with SparkTestSession {

  def uid: String = java.util.UUID.randomUUID.toString.replaceAll("-", "")

  val piplineDirectoryName: String = s"dataGround|pipeline"
  val pipelineDirectory: FileSystemLocation = FileSystemLocation(piplineDirectoryName)

  val outputDirectoryName: String = s"testTarget|$uid/result.parquet"
  val outputDirectory: FileSystemLocation = FileSystemLocation(outputDirectoryName)

  it should "successfully run Job" in {

    val arguments = Seq(
      "--name", "job",
      "--pipeline", piplineDirectoryName,
      "--output", outputDirectoryName,
    )

    // The last argument is not necessary, it is explicit passed here to show that the
    // sessionFactory from the SparkTestSession is used.
    val result = new PipelineSparkJob().execute(arguments)(Some(sessionFactory))

    result.isSuccess shouldEqual true

    pipelineDirectory.locate(DataPaths.Tier1.ingestedData).exists() shouldBe true
    pipelineDirectory.locate(DataPaths.Tier2.corpus).exists() shouldBe true

    spark.read.parquet(pipelineDirectory.locate(DataPaths.Tier1.ingestedData).resolve.toString).count() shouldEqual 6
    spark.read.parquet(pipelineDirectory.locate(DataPaths.Tier2.corpus).resolve.toString).count() shouldEqual 3
    spark.read.parquet(outputDirectory.resolve.toString).count() shouldEqual 3

  }

  override def afterAll(): Unit = {
    super.afterAll()
    pipelineDirectory.locate(DataPaths.Tier1.ingestedData).delete()
    pipelineDirectory.locate(DataPaths.Tier2.corpus).delete()
    outputDirectory.delete()
  }

}
