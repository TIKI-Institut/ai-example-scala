package com.tiki.examples.scala.jobs


import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class HadoopClientAPITestJobSpec
  extends AnyFlatSpec
  with Matchers
  with BeforeAndAfterAll {

  it should "work if no arguments are passed" in {
    new HadoopClientAPITestJob().execute(Seq()).isSuccess.shouldEqual(true)
  }

  it should "work is a name is passed" in {
    new HadoopClientAPITestJob().execute(Seq("--name", "someName")).isSuccess.shouldEqual(true)
  }

}
