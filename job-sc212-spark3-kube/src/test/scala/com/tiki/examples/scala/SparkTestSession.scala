package com.tiki.examples.scala

import com.tiki.ai.hadoop.HadoopConfigHolder
import com.tiki.ai.spark.config.SparkSessionConfigHolder
import com.tiki.ai.spark.{SharedSparkSessionFactory, SparkSessionFactory}
import org.apache.spark.sql.SparkSession

/**
 * Our SparkSession for unittests. Just import the sessionFactory in each unittest, that needs a SparkSession
 * */
trait SparkTestSession extends SparkSessionConfigHolder with HadoopConfigHolder {
  implicit val sessionFactoryOption: Option[SparkSessionFactory] = Some(new SharedSparkSessionFactory())
  val sessionFactory:SparkSessionFactory = sessionFactoryOption.get

  updateSparkConfig(_.setMaster("local[*]"))
  updateSparkConfig(_.setAppName("TestSparkJobs"))
  updateSparkConfig(_.set("spark.driver.host", "127.0.0.1"))
  updateSparkConfig(_.set("spark.default.parallelism", "4"))
  updateSparkConfig(_.set("spark.sql.shuffle.partitions", "4"))

  implicit val spark: SparkSession = sessionFactory.createSession(this)
}