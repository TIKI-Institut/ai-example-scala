scalaVersion := "2.12.18"

organization := "com.tiki"

name := "web-sc212-spark3"
version := "0.1.5"

libraryDependencies += "com.tiki"         %% "ai-common" % "1.2.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.4.1" % Provided

resolvers ++= Seq(
  "DSP-public" at "https://nexus.tiki-dsp.io/repository/maven-public/"
)

updateOptions := updateOptions.value.withLatestSnapshots(true)

Compile / doc / sources := Seq.empty

enablePlugins(JavaAppPackaging)
