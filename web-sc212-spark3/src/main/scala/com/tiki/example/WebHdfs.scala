package com.tiki.example

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives.{complete, path}
import akka.http.scaladsl.server.{Directive0, Route}
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.bin.webKernel.WebResource
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SparkSession

/**
  * Test hdfs access
  */
class HdfsEndpoint extends WebResource {
  override def endpoint: Directive0 = path("test_hdfs")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {

    val spark: SparkSession = SparkSession
      .builder()
      .config("spark.ui.enabled", "false")
      .config("spark.testing", "true")
      .config("spark.testing.memory", (256 * 1024 * 1024).toString)
      .config("spark.driver.host", "127.0.0.1")
      .master("local[*]")
      .getOrCreate()

    val fileName = "hdfs:///tiki-test/tests/web-hdfs-test.txt"
    val dfsPath = new Path(fileName)

    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    if (fs.exists(dfsPath)) {
      println("Path already exists, delete it first")
      fs.delete(dfsPath, true)
    }

    val fileRDD = spark.sparkContext.parallelize(List("Test web hdfs read write"))
    fileRDD.saveAsTextFile(fileName)

    val readFileRDD = spark.sparkContext.textFile(fileName)

    val content = readFileRDD.collect().mkString("")
    HttpResponse(200).withEntity(s"File content: $content")
  }
}
