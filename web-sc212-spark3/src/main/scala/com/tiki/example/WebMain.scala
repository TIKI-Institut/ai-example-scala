package com.tiki.example

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.Directives.{complete, path}
import com.tiki.ai.authentication.RequiredRoles
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.bin.webKernel.WebResource
import org.apache.spark.sql.SparkSession

/**
 * Sample shows several custom endpoints
 * which is defined in our oauth2-adapter lib
 */
class OpenEndpoint extends WebResource {
  override def endpoint: Directive0 = path("test_open")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    HttpResponse(200).withEntity("This is an open endpoint from the main with no login required")
  }
}


@RequiredRoles(roles = Array("capability-1"))
class SecuredSparkEndpoint extends WebResource {

  override def endpoint: Directive0 = path("test_secured_spark3_with_roles")

  val spark: SparkSession = SparkSession
    .builder()
    .config("spark.ui.enabled", "false")
    .config("spark.testing", "true")
    .config("spark.testing.memory", (256 * 1024 * 1024).toString)
    .config("spark.driver.host", "127.0.0.1")
    .master("local[*]")
    .getOrCreate()

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    HttpResponse(200).withEntity(s"This is a secured endpoint with Spark ${spark.sparkContext.version}")
  }
}


