package com.tiki.examples.scala.jobs

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.util.{Success, Try}

class DeployJobWithCliArgsSpec
  extends AnyFlatSpec
    with Matchers
    with BeforeAndAfterAll {

  // This test demonstrates how to write a unit test for an job
  // that uses the DeploymentServerClient to deploy another job.
  // The deploy method is overwritten, in order one can test other possible
  // functionalities of the job.

  it should "successfully run Job" in {

    val job = new DeployJobWithCliArgs {
      override def principal: String     = "DSP_PRINCIPAL"
      override def project: String       = "DSP_NAME"
      override def environment: String   = "DSP_ENVIRONMENT"
      override def flavorVersion: String = "DSP_FLAVOR_VERSION"

      override def deploy(flavorName: String,
                          flavorVersion: String = flavorVersion,
                          principal: String = principal,
                          project: String = project,
                          environment: String = environment,
                          deploymentServerBaseUrl: String,
                          deploymentServerVersion: String,
                          token: String,
                          overrideJson: Option[String]): Try[(Int, String)] = {
        Success((200, "success"))
      }
      override def getTokenFromFile(filename: String = "/dsp/auth/token"): String = {
        "test"
      }
    }

    val piplineDirectoryName: String = s"dataGround|pipeline"
    val outputDirectoryName: String = s"testTarget|result.parquet"

    val arguments = Seq(
      "--name", "job",
      "--pipeline", piplineDirectoryName,
      "--output", outputDirectoryName,
    )

    val result = job.execute(arguments)

    result.isSuccess shouldEqual true

    // Other functionalities can be tested here.

  }
}
