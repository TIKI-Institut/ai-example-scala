package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.job.JobConfig
import scopt.OptionParser

case class InsecureJobWithCliArgsConfig(name: String)
  extends OptionParser[InsecureJobWithCliArgsConfig](name)
    with JobConfig {
  opt[String]('n', "name") optional() valueName "<name>" action { (x, c) =>
    c.copy(name = x)
  } text "Job name"
}
