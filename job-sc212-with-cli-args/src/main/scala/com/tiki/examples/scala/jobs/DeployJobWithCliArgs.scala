package com.tiki.examples.scala.jobs

import com.tiki.ai.deploymentServerClient.{OverrideJsonSupport, DeploymentServerClient}
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.job.{CLIOptionParserRunner, Job, OptionParserRunner}
import com.tiki.examples.scala.jobConfigParser.DeployJobWithCliArgsConfig

import java.util.logging.{Level, Logger}
import scala.util.{Failure, Success, Try}


object DeployJobWithCliArgs extends OptionParserRunner {
  override def apply(arguments: Seq[String]): Try[Unit] = new DeployJobWithCliArgs().execute(arguments)

}


class DeployJobWithCliArgs
  extends Job[DeployJobWithCliArgsConfig]
  with CLIOptionParserRunner[DeployJobWithCliArgsConfig]
  with OverrideJsonSupport
  with DeploymentServerClient {

  var myJobName: String = _

  /**
   * Main method that executes business logic of the job
   */
  override def run(jobConfig: DeployJobWithCliArgsConfig): Unit = {
    myJobName = jobConfig.name

    val log = setupLogger("LOG_LEVEL")
    log.info("Starting Deployment-Job")
        log.info(s"name ${jobConfig.name}")
        log.info(s"pipeline ${jobConfig.pipeline}")
        log.info(s"output ${jobConfig.output}")

    val pipelineJob = "job-sc212-spark3-kube-pipeline"
    val pipelineJobVersion = "0.0.4"
    val deploymentServerVersion = sys.env.getOrElse("DEPLOYMENT_SERVER_VERSION", "latest")

    val pipelineJobArgs: Map[String, String] = Map(
      ("--name", "pipeline"),
      ("--pipeline", jobConfig.pipeline.resolve.toString),
      ("--output", jobConfig.output.resolve.toString))

    val pipelineReportJobOverride = buildJobOverrideJsonString(
      jobName = "com.tiki.examples.scala.jobs.PipelineSparkJob",
      postfix = "-deployed-from-job",
      jobArgs = pipelineJobArgs)

    log.info(f"Deploying the $pipelineJob with the version $pipelineJobVersion")
    val pipelineReportDeployResponse = deploy(flavorName = pipelineJob,
                                              flavorVersion = pipelineJobVersion,
                                              deploymentServerVersion = deploymentServerVersion,
                                              overrideJson = Some(pipelineReportJobOverride))

    pipelineReportDeployResponse match {
      case Success(_) =>
        log.info(f"Job deployment successful!")
      case Failure(e) =>
        log.warning(s"Deployment of the job failed! aborting with exception")
        throw e
    }
  }

  def setupLogger(env: String): Logger = {
    val log = Logger.getLogger(DeployJobWithCliArgs.toString)
    log.setLevel(Level.INFO)

    sys.env.get(env) match {
      case Some(logLevelString) => try {
        log.setLevel(Level.parse(logLevelString))
      } catch {
        case _: IllegalArgumentException => log.warning(s"Received invalid log level value '$logLevelString'. Continue with default log level INFO")
      }
      case None => log.warning(s"Environment variable '$env' did not return any value. Continue with default log level INFO")
    }

    log.info(s"current log level: ${log.getLevel}")
    log
  }

  def parse(arguments: Seq[String]): DeployJobWithCliArgsConfig = {
    val initialConfig = DeployJobWithCliArgsConfig()
    initialConfig.withArguments(arguments)(initialConfig.parser)
  }

}



