package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.ai.job.JobConfig
import scopt.OptionParser


case class DeployJobWithCliArgsConfig(name: String = "DeployJob",
                                      pipeline: FileSystemLocation = FileSystemLocation.empty,
                                      output: FileSystemLocation = FileSystemLocation.empty)
extends JobConfig {

  val parser: OptionParser[DeployJobWithCliArgsConfig] = new OptionParser[DeployJobWithCliArgsConfig]("DeployJob")
  {
    opt[String]('n', "name").optional().valueName("<name>").action( (x, c) =>  c.copy(name = x))
      .text("Job name")

    opt[String]('p', "pipeline").optional().valueName("<pipeline>").action( (x, c) =>  c.copy(pipeline = FileSystemLocation(x)))
      .text("pipeline directory")

    opt[String]('o', "output").optional().valueName("<output>").action( (x, c) =>  c.copy(output = FileSystemLocation(x)))
      .text("output directory")
  }
}
