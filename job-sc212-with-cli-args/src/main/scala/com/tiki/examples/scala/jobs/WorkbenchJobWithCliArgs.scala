package com.tiki.examples.scala.jobs

import com.tiki.ai.dw.{DigitalizationWorkbenchClient, FileBearerContext}
import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import com.tiki.ai.job.{CLIOptionParserRunner, Job, OptionParserRunner}
import com.tiki.examples.scala.jobConfigParser.WorkbenchJobWithCliArgsConfig

import java.io.{BufferedWriter, File, FileWriter}
import java.util.logging.{Level, Logger}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._


object WorkbenchJobWithCliArgs extends OptionParserRunner {
  override def apply(arguments: Seq[String]): Try[Unit] = new WorkbenchJobWithCliArgs().execute(arguments)
}

class WorkbenchJobWithCliArgs
  extends Job[WorkbenchJobWithCliArgsConfig]
  with CLIOptionParserRunner[WorkbenchJobWithCliArgsConfig] {

  var myJobName: String = _

  /**
   * Main method that executes business logic of the job
   */
  override def run(jobConfig: WorkbenchJobWithCliArgsConfig): Unit = {
    myJobName = jobConfig.name

    val log = setupLogger("LOG_LEVEL")

    log.info("Starting Workbench-Job")
    log.info(s"name ${jobConfig.name}")
    log.info(s"source ${jobConfig.source}")
    log.info(s"destination ${jobConfig.destination}")
    log.info(s"token ${FileBearerContext.token}")

    val serviceURL = "https://workbench.tiki-test.tiki-dsp.io/tiki-test"
    val client = DigitalizationWorkbenchClient(serviceURL=serviceURL)
    val baseFolder: String = "hdfs:/ai-example/dw/scala-client-test"

    log.info(s"url   : $serviceURL" + s"/list")
    log.info(s"source: $baseFolder")

    val fs = client.fs()(FileBearerContext)
    fs match {
      case Success(listResults) => listResults.foreach(log.info)
      case Failure(f) => throw new Exception(s"calling 'fs' failed: ${f.toString}")
    }

    val resultDownload = client.download(s"$baseFolder/src_download/download.txt")(FileBearerContext)
    resultDownload match {
      case Success(s) => log.info("Download succeeded")
      case Failure(f) => throw new Exception(s"calling 'download' failed: ${f.toString}")
    }

    val file = new File("example_upload_file.txt")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write("line\nanother line")
    bw.close()
    val files = Seq(new File("example_upload_file.txt"))

    val uploadResult = client.upload(s"$baseFolder/dst_upload/", files, overwrite = true)(FileBearerContext)
    uploadResult match {
      case Success(s) => log.info(s.toString)
      case Failure(f) => throw new Exception(s"calling 'upload' failed: ${f.toString}")
    }

    val copyResult = client.copy(s"$baseFolder/dst_upload/example_upload_file.txt",
                              s"$baseFolder/dst_copy/example_upload_file.txt",
                                         wait = true, updateWait = 2.seconds)(FileBearerContext)
    copyResult match {
      case Success(s) => log.info(s.toString)
      case Failure(f) => throw new Exception(s"calling 'copy' failed: ${f.toString}")
    }

    val moveResult = client.move(s"$baseFolder/dst_copy/example_upload_file.txt",
                               s"$baseFolder/dst_move/example_upload_file.txt",
                                  wait = true, updateWait = 2.seconds)(FileBearerContext)
    moveResult match {
      case Success(s) => log.info(s.toString)
      case Failure(f) => throw new Exception(s"calling 'move' failed: ${f.toString}")
    }

    val mkDirResult = client.mkDir(s"$baseFolder/newDir")(FileBearerContext)
    mkDirResult match {
      case Success(s) => log.info(s.toString)
      case Failure(f) => throw new Exception(s"calling 'mkDir' failed: ${f.toString}")
    }

    val deleteResult = client.delete(s"$baseFolder/newDir")(FileBearerContext)
    deleteResult match {
      case Success(s) => log.info(s.toString)
      case Failure(f) => throw new Exception(s"calling 'delete' failed: ${f.toString}")
    }

    val resultList = client.list(baseFolder)(FileBearerContext)
    resultList match {
      case Success(listResults) => listResults.foreach(r => log.info(r.name))
      case Failure(f) => throw new Exception(s"calling 'list' failed: ${f.toString}")
    }

  }

  def setupLogger(env: String): Logger = {
    val log = Logger.getLogger(WorkbenchJobWithCliArgs.toString)
    log.setLevel(Level.INFO)

    sys.env.get(env) match {
      case Some(logLevelString) => try {
        log.setLevel(Level.parse(logLevelString))
      } catch {
        case _: IllegalArgumentException => log.warning(s"Received invalid log level value '$logLevelString'. Continue with default log level INFO")
      }
      case None => log.warning(s"Environment variable '$env' did not return any value. Continue with default log level INFO")
    }

    log.info(s"current log level: ${log.getLevel}")
    log
  }

  def parse(arguments: Seq[String]): WorkbenchJobWithCliArgsConfig = {
    val initialConfig = WorkbenchJobWithCliArgsConfig()
    initialConfig.withArguments(arguments)(initialConfig.parser)
  }

}
