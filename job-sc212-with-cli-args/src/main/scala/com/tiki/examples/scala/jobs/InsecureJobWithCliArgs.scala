package com.tiki.examples.scala.jobs

import com.tiki.ai.job.{CLIOptionParserRunner, Job, OptionParserRunner}
import com.tiki.examples.scala.jobConfigParser.InsecureJobWithCliArgsConfig

import scala.util.Try


object InsecureJobWithCliArgs extends OptionParserRunner {
    override def apply(arguments: Seq[String]): Try[Unit] = new InsecureJobWithCliArgs().execute(arguments)

}


class InsecureJobWithCliArgs
  extends Job[InsecureJobWithCliArgsConfig]
  with CLIOptionParserRunner[InsecureJobWithCliArgsConfig] {

  var myJobName: String = _

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: InsecureJobWithCliArgsConfig): Unit = {
    myJobName = jobConfig.name
  }

  override def parse(arguments: Seq[String]): InsecureJobWithCliArgsConfig =
    InsecureJobWithCliArgsConfig("InsecureJobWithArgs")
}
