package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.fs.FileSystemLocation
import com.tiki.ai.job.JobConfig
import scopt.OptionParser


case class WorkbenchJobWithCliArgsConfig(name: String = "DeployJob",
                                         source: FileSystemLocation = FileSystemLocation.empty,
                                         destination: FileSystemLocation = FileSystemLocation.empty)
extends JobConfig {

  val parser: OptionParser[WorkbenchJobWithCliArgsConfig] = new OptionParser[WorkbenchJobWithCliArgsConfig]("DeployJob")
  {
    opt[String]('n', "name").optional().valueName("<name>").action( (x, c) =>  c.copy(name = x))
      .text("Job name")

    opt[String]('s', "source").optional().valueName("<source>").action( (x, c) =>  c.copy(source = FileSystemLocation(x)))
      .text("source directory")

    opt[String]('d', "destination").optional().valueName("<destination>").action( (x, c) =>  c.copy(destination = FileSystemLocation(x)))
      .text("destination directory")
  }
}
