scalaVersion := "2.12.18"

organization := "com.tiki"

name := "job-sc212-with-cli-args"
version := "0.1.5"

libraryDependencies += "com.tiki"         %% "ai-common" % "1.2.0"
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "3.3.4"
libraryDependencies += "org.scalatest"  %% "scalatest" % "3.2.15" % Test

resolvers ++= Seq(
  "DSP-public" at "https://nexus.tiki-dsp.io/repository/maven-public/"
)

updateOptions := updateOptions.value.withLatestSnapshots(true)

Compile / doc / sources := Seq.empty

enablePlugins(JavaAppPackaging)
