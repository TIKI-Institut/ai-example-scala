scalaVersion := "2.12.18"

maintainer := "info@tiki-institut.com"
organization := "com.tiki"
name := "job-sc212"
version := "0.1.5"

libraryDependencies += "com.tiki"       %% "ai-common" % "1.2.0"
libraryDependencies += "org.scalatest"  %% "scalatest" % "3.2.15" % Test

resolvers ++= Seq(
  "DSP-public" at "https://nexus.tiki-dsp.io/repository/maven-public/"
)

updateOptions := updateOptions.value.withLatestSnapshots(true)

Compile / doc / sources := Seq.empty

enablePlugins(JavaAppPackaging)
