package com.tiki.examples.scala.jobConfigParser

import com.tiki.ai.job.JobConfig

case class InsecureJobConfig(name: String) extends JobConfig
