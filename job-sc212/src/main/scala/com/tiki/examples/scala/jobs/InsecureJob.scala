package com.tiki.examples.scala.jobs

import com.tiki.ai.job.{CLIOptionParserRunner, Job, OptionParserRunner}
import com.tiki.examples.scala.jobConfigParser.InsecureJobConfig

import java.util.logging.{Level, Logger}
import scala.util.Try

object InsecureJob extends OptionParserRunner {
  def apply(arguments: Seq[String]): Try[Unit] = new InsecureJob().execute(arguments)
}


class InsecureJob
  extends Job[InsecureJobConfig]
  with CLIOptionParserRunner[InsecureJobConfig] {

  var myJobName: String = _

  /**
   * Main method that executes business logic of the job
   */
  override def run(jobConfig: InsecureJobConfig): Unit = {
    val log = setupLogger("LOG_LEVEL")
    log.info("Hello World (from insecure job)")
    myJobName = jobConfig.name
  }

  def setupLogger(env: String): Logger = {
    val log = Logger.getLogger(InsecureJob.toString)
    log.setLevel(Level.INFO)

    sys.env.get(env) match {
      case Some(logLevelString) => try {
        log.setLevel(Level.parse(logLevelString))
      } catch {
        case _: IllegalArgumentException => log.warning(s"Received invalid log level value '$logLevelString'. Continue with default log level INFO")
      }
      case None => log.warning(s"Environment variable '$env' did not return any value. Continue with default log level INFO")
    }

    log.info(s"current log level: ${log.getLevel}")
    log
  }

  def parse(arguments: Seq[String]): InsecureJobConfig = InsecureJobConfig("InsecureJob")
}