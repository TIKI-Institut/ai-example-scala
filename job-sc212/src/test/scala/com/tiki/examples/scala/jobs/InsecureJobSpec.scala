package com.tiki.examples.scala.jobs

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class InsecureJobSpec
  extends AnyFlatSpec
  with Matchers
  with BeforeAndAfterAll{

  it should "successfully run Job" in {
    InsecureJob(Seq.empty[String]).isSuccess.shouldEqual(true)
  }
}
