@Library(value='tiki/jenkins@ZOLINFRA-21', changelog=false) _

pipeline {
    agent {
        kubernetes {
            defaultContainer 'scala212'
            yamlMergeStrategy merge()
            inheritFrom 'dsp-scala-212 dsp-provision dsp-integration-test'
            yaml """
                apiVersion: v1
                kind: Pod
                metadata:
                spec:
                  containers:
                  - name: provisioner
                    image: docker.tiki-dsp.io/cicd-provisioner:${params.PROVISIONER_VERSION}
                """
        }
    }

    options {
        buildDiscarder(logRotator(daysToKeepStr: '14'))
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    parameters {
        string(name: 'KERNEL_VERSION', defaultValue: 'latest', description: 'ai-kernel version')
        string(name: 'PROVISIONER_VERSION', defaultValue: 'latest', description: 'cicd-provisioner version')
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    environment {
        PRINCIPAL   = 'tiki-test'
        PROJECT     = 'ai-example-scala'
        ENVIRONMENT = aiExampleEnvironment(env.BRANCH_NAME, params.KERNEL_VERSION, params.PROVISIONER_VERSION)
        PROVISIONER_DSPDOMAIN = 'tiki-dsp.tech'
    }

    stages {
        stage('Prepare') {
            steps {
                //bitbucketStatusNotify(buildState: 'INPROGRESS')
                 echo "Starting pipeline for branch '${env.BRANCH_NAME}' with KERNEL_VERSION '${params.KERNEL_VERSION}' and PROVISIONER_VERSION '${params.PROVISIONER_VERSION}'"
            }
        }
        stage('Build') {
            steps {
                container('scala212') {
                    dir('job-sc212') {
                        sh "sbt clean compile"
                    }
                    dir('job-sc212-spark3-kube') {
                        sh "sbt clean compile"
                    }
                    dir('job-sc212-with-cli-args') {
                        sh "sbt clean compile"
                    }
                }

            }
        }
        stage('Test') {
            steps {
                container('scala212') {
                    dir('job-sc212') {
                        sh "sbt test"
                    }
                    dir('job-sc212-spark3-kube') {
                        sh "sbt test"
                    }
                    dir('job-sc212-with-cli-args') {
                        sh "sbt test"
                    }
                }
            }
        }
        stage('Provisioner') {
            stages {
                stage('prepare') {
                    steps {
                        container('provisioner') {
                            script {
                                sh "mkdir -p /home/jenkins/backup"
                                generateCiMetaFile("${params.KERNEL_VERSION}", "${ENVIRONMENT}")
                            }
                        }
                    }
                }
                stage('decommission') {
                    steps {
                        container('provisioner') {
                            script {
                                try {
                                    sh "ai-provisioner -p /opt/.provisioner decommission ${WORKSPACE}"
                                } catch (ex) {
                                    echo 'Decommission failed, skipping...'
                                }
                            }
                        }
                    }
                }
                stage('setup project') {
                    steps {
                        container('provisioner') {
                            sh "ai-provisioner -p /opt/.provisioner setup ${WORKSPACE}"
                        }
                    }
                }
                stage('assign keycloak client roles') {
                    options { skipDefaultCheckout() }
                    steps {
                        container('keycloak') {
                            sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.tech/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                            sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT} --rolename dsp-deploy --rolename dsp-undeploy --rolename dsp-list --rolename dsp-decommission --rolename dsp-provision"
                        }
                    }
                }
                stage('provision') {
                    steps {
                        container('provisioner') {
                            sh "ai-provisioner -p /opt/.provisioner provision ${WORKSPACE}"
                        }
                    }
                }
                stage('deploy') {
                    steps {
                        container('provisioner') {
                            // build override json with the DEPLOYMENT_SERVER_VERSION envVar (needed for the "job-sc212-deploy-job" flavor)
                            sh """echo "[{\\"envVars\\": [{\\"name\\":\\"DEPLOYMENT_SERVER_VERSION\\",\\"value\\":\\"${params.PROVISIONER_VERSION}\\"}]}]" > ./depl-server-version-override.json"""

                            //Deploy all flavors with the override json -> easier to deploy all flavors with a single command & more error-proof
                            //If you want only override a single flavor, you have run the deploy command for every flavor with environment, version, project, flavor name, etc.
                            //which means that the version for every flavor has to be updated after provisioning a new one
                            //-> potential for error (if someone forgets to update the versions here, the Jenkins build runs old flavor versions and maybe succeeds by mistake)
                            sh "ai-provisioner -p /opt/.provisioner --override ./depl-server-version-override.json deploy ${WORKSPACE}"
                        }
                    }
                }
                stage('check job status') {
                    steps {
                        container('kubectl') {
                            script {
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-with-cli-args")
                                //checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-deploy-job") ## interacts with PROD Deployment server
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-spark3-kube")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-spark3-kube-hdfs")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "train-job-sc212-spark3-kube")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-spark3-kube-hdfs-client-api")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-spark3-kube-pipeline")
                                //checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-sc212-spark3-kube-pipeline-deployed-from-job") ## will not be deployed in DEV env
                            }
                        }
                    }
                }
                stage('check web services') {
                    options { skipDefaultCheckout() }
                    steps {
                        container('keycloak') {
                            script {
                                // get roles to use secure endpoints
                                sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.tech/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                                sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-web-sc212 --rolename capability-1"
                                sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-web-sc212-spark3 --rolename capability-1"
                                // check open endpoint
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-sc212/test_open", 5)
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-sc212-spark3/test_open", 5)
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-sc212-spark3/test_hdfs", 5)
                                // get token for secure endpoint and save it in access_token.json
                                ACCESS_TOKEN = sh (
                                    script: "set +x \n curl --location --request POST 'https://auth.tiki-dsp.tech/auth/realms/${PRINCIPAL}/protocol/openid-connect/token' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'client_id=${PROJECT}-${ENVIRONMENT}' --data-urlencode 'grant_type=password' --data-urlencode username=\$(cat /opt/kc/credentials.json | jq -r .username) --data-urlencode password=\$(cat /opt/kc/credentials.json | jq -r .password) | jq -r .access_token |tr -d '\\n'",
                                    returnStdout: true
                                ).trim()
                                // check secure endpoints
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-sc212/test_secured_with_roles", bearerToken: "${ACCESS_TOKEN}", 5)
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-sc212-spark3/test_secured_spark3_with_roles", bearerToken: "${ACCESS_TOKEN}", 5)
                            }
                        }
                    }
                }
                stage('check custom CA certs') {
                    steps {
                        container('kubectl') {
                            script {
                                checkCaCertInLinuxStore(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-sc212", certCommonName: "test-certificate-for-examples")
                                checkCaCertInLinuxStore(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-sc212-spark3", certCommonName: "test-certificate-for-examples")
                                checkCaCertInJavaKeyStore(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-sc212-spark3", certCommonName: "test-certificate-for-examples")
                            }
                        }
                    }
                }
                stage('Check DNS service') {
                    steps {
                        container('kubectl') {
                            script {
                                projectParams = [principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-sc212"]
                                installNslookup(projectParams + [hostname: "tiki-institut.com"])
                                nslookup( projectParams + [hostname: "tiki-institut.com"])
                                nslookup( projectParams + [hostname: "intern.test-dns"])
                                nslookup( projectParams + [hostname: "jupyterhub.tiki-test.svc.cluster.local"])
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        failure {
            doFailureSteps(slackNotification: true, bitbucketNotification: false)
        }
        fixed {
            doFixedSteps(slackNotification: true, bitbucketNotification: false)
        }
    }
}
