scalaVersion := "2.12.18"

organization := "com.tiki"

name := "web-sc212"
version := "0.1.5"

libraryDependencies += "com.tiki"         %% "ai-common" % "1.2.0"

resolvers ++= Seq(
  "DSP-public" at "https://nexus.tiki-dsp.io/repository/maven-public/"
)

updateOptions := updateOptions.value.withLatestSnapshots(true)

Compile / doc / sources := Seq.empty

enablePlugins(JavaAppPackaging)

Compile / run / mainClass := Some("com.tiki.ai.bin.webKernel.Main")
