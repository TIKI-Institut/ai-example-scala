package com.tiki.example

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive0, Route}
import com.tiki.ai.authentication.RequiredRoles
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.bin.webKernel.WebResource

import scala.util.Properties

/**
  * Sample shows several custom endpoints
  * which is defined in our oauth2-adapter lib
  */
class OpenEndpoint extends WebResource {
  override def endpoint: Directive0 = path("test_open")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    HttpResponse(200).withEntity("This is an open endpoint from the main with no login required")
  }
}

class ResourceEndpoint extends WebResource {
  override def endpoint: Directive0 = path("resource_limits")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    val cpuLimit = Properties.envOrElse("CPU_LIMIT", "unavailable")
    val memoryLimit = Properties.envOrElse("MEMORY_LIMIT", "unavailable")
    HttpResponse(200).withEntity(s"cpu limit: $cpuLimit m, memory limit $memoryLimit Mi")
  }
}

@RequiredRoles()
class SecuredEndpoint extends WebResource {
  override def endpoint: Directive0 = path("test_secured")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    HttpResponse(200).withEntity("This endpoint from main need a login")
  }
}

@RequiredRoles(roles = Array("capability-1"))
class SecuredEndpointWithRoles extends WebResource {
  override def endpoint: Directive0 = path("test_secured_with_roles")

  override def route(oidcToken: Option[OidcToken] = None): Route = complete {
    oidcToken match {
      case Some(token) =>
        HttpResponse(200).withEntity(s"This endpoint from main need a login and an assigned role and was valid for client: ${token.oauthClient}")
      case _ =>
        HttpResponse(403).withEntity("Insufficient rights.")
    }

  }
}
